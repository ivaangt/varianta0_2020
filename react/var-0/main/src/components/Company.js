import React, { Component } from 'react'

class Company extends Component{
    
     handleClickButton = (event)=>{
        this.props.onDelete(this.props.item.id);
    }
    
    
    render(){
        return(
            <div>
                "Company {this.props.item.name}" 
                 <input type = "button" value = "delete" onClick = { this.handleClickButton}/>
            </div>
            )
    }
}

export default Company
