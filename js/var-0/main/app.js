function bowdlerize(input, dictionary){
    if(typeof input !=='string'){
        throw new Error(`Input should be a string`);
    }
    for(let i of dictionary){
        if(typeof i !== 'string'){
             throw new Error(`Invalid dictionary format`);
        }
    }
    
    var cuvinte = input.split(' ');
    
    for(let i =0; i < cuvinte.length; i++){
        for(let y = 0; y < dictionary.length;y++){
            if(cuvinte[i].toLowerCase() === dictionary[y]){
                let word = cuvinte[i].substring(1,cuvinte[i].length-1);
              
                let star = "*".repeat(word.length);
                let modif = cuvinte[i].replace(word,star);
                cuvinte[i] = modif;
            }
        }
    }
    input = cuvinte.join(" ");
   return input;
}

const app = {
    bowdlerize
};

module.exports = app;